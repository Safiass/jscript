var userInput;

function fromNumToLetters(number){

	if(isNaN(number) || number < 0 || number> 999) {

		return 'error';
	} 

	var unitLetters = ['', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf'];

	var dizLetters = ['', 'dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante', 'quatre-vingt', 'quatre-vingt'];

	var units = number%10;  // test of 471 : 1

	var diz = (number%100 - units)/10; // test of 471 : 7

	var cent = (number%1000 - number%100)/100; //test of 471 : 4

	var unitOutput, dizOutput, centOutput;

	if (number === 0) {

		return 'Zero'

	} else {

        
        unitOutput = (units === 1 && diz > 0 && diz !== 8 ? 'et-' : '') + unitLetters[units];

        if (diz === 1 && units > 0) {

            dizOutput = unitLetters[10 + units];
            unitOutput = '';

        } else if (diz === 7 || diz === 9) {

            dizOutput = dizLetters[diz] + '-' + (diz === 7 && units === 1 ? 'et-' : '') + unitLetters[10 + units];
            unitOutput = '';

        } else {

            dizOutput = dizLetters[diz];

        }

        dizOutput += (units === 0 && diz === 8 ? 's' : '');

        centOutput = (cent > 1 ? unitLetters[cent] + '-' : '') + (cent > 0 ? 'cent' : '') + (cent > 1 && diz == 0 && units == 0 ? 's' : '');

       
        return centOutput + (centOutput && dizOutput ? '-' : '') + dizOutput + (centOutput && unitOutput || dizOutput && unitOutput ? '-' : '') + unitOutput;
    }


}


while (userInput = prompt('Enter a number between 0 and 999 : ')) {
	alert(fromNumToLetters(parseInt(userInput, 10)));
}